/**
 * @file	main.cpp
 * @brief	Codigo fonte de teste das funcoes para calculo das medidas
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "calcarea.h"
#include "calcperimetro.h"
#include "calcvolume.h"


/** @brief Funcao principal */
int main() {
    while (true) {
		cout << endl;
        cout << "Calculadora de Geometria Plana e Espacial" << endl;
        cout << "(1) Triangulo equilatero" << endl;
        cout << "(2) Retangulo" << endl;
        cout << "(3) Quadrado" << endl;
        cout << "(4) Circulo" << endl;
        cout << "(5) Piramide com base quadrangular" << endl;
        cout << "(6) Cubo" << endl;
        cout << "(7) Paralelepipedo" << endl;
        cout << "(8) Esfera" << endl;
        cout << "(0) Sair" << endl << endl;

		int opcao;      				// Opcao a ser escolhida pelo usuario
        cout << "Digite a sua opcao: ";
        cin >> opcao;

        switch(opcao) {
			case 1:
				float lt;						// Lado
				cout << "Digite o tamanho dos lados do triangulo: ";
				cin >> lt;

				calc_area_triangulo(lt);		// Calculo da area
				calc_perimetro_triangulo(lt);	// Calculo do perimetro
                break;
			case 2:
				float br;							// Base
				float ar;							// Altura
				cout << "Digite o tamanho da base do retangulo: ";
				cin >> br;
				cout << "Digite o tamanho da altura do retangulo: ";
				cin >> ar;

				calc_area_retangulo(br, ar);		// Calculo da area
				calc_perimetro_retangulo(br, ar);	// Calculo do perimetro
				break;
			case 3:
				float lq;						// Lado
				cout << "Digite o tamanho dos lados do quadrado: ";
				cin >> lq;
				calc_area_quadrado(lq);			// Calculo da area
				calc_perimetro_quadrado(lq);	// Calculo do perimetro
				break;
			case 4:
				float rc;								// Raio
				cout << "Digite o tamanho do raio do circulo: ";
				cin >> rc;
				calc_area_circulo(rc);					// Calculo da area
				calc_comprimento_circunferencia(rc);	// Calculo do perimetro
				break;
			case 5:
				float abase;					// Aresta da base
				float apt;						// Apotema
				float altp;						// Altura
				cout << "Digite o tamanho da aresta da base da piramide: ";
				cin >> abase;
				cout << "Digite o tamanho do apotema da piramide: ";
				cin >> apt;
				cout << "Digite o tamanho da altura da piramide: ";
				cin >> altp;
				calc_area_piramide(abase, apt);		// Calculo da area
				calc_volume_piramide(abase, altp);	// Calculo do volume
				break;
			case 6:
				float ac;				// Aresta
				cout << "Digite o tamanho das arestas do cubo: ";
				cin >> ac;
				calc_area_cubo(ac);		// Calculo da area
				calc_volume_cubo(ac);	// Calculo do volume
				break;
			case 7:
				float a1, a2, a3;						// Arestas
				cout << "Digite o tamanho das arestas do paralelepipedo: ";
				cin >> a1 >> a2 >> a3;
				calc_area_paralelepipedo(a1, a2, a3);	// Calculo da area
				calc_volume_paralelepipedo(a1, a2, a3);	// Calculo do volume
				break;
			case 8:
				float re;					// Raio
				cout << "Digite o tamanho do raio da esfera: ";
				cin >> re;
				calc_area_supesferica(re);	// Calculo da area
				calc_volume_esfera(re);		// Calculo do volume
				break;				
            case 0:
                return 0;
            default:
                cout << "Opcao invalida. Por favor, tente novamente." << endl << endl;
        }
    }

	return 0;
}
