/**
 * @file	area.cpp
 * @brief	Codigo fonte com a implementacao de funcoes que calculam a area 
 *			de figuras geometricas planas e espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#include "area.h"


/**
 * @brief Calcula a area de um triangulo
 * @param b Tamanho da base do triangulo
 * @param h Tamanho da altura do triangulo
 * @return Area do triangulo
 */
float area_triangulo(float b, float h) {
	return (b * h) / 2;
}


/**
 * @brief Calcula a area de um retangulo
 * @param b Tamanho da base do triangulo
 * @param h Tamanho da altura do triangulo
 * @return Area do retangulo
 */
float area_retangulo(float b, float h) {
    return b * h;
}


/**
 * @brief Calcula a area de um quadrado
 * @param l Tamanho do lado do quadrado
 * @return Area do quadrado
 */
float area_quadrado(float l) {
    return pow(l, 2);
}


/**
 * @brief Calcula a area de um circulo
 * @param r Tamanho do raio do circulo
 * @return Area do circulo
 */
float area_circulo(float r) {
    return PI * pow(r, 2);
}


/**
 * @brief Calcula a area de uma piramide
 * @param ab Area da base da piramide
 * @param al Area lateral da piramide
 * @return Area da piramide
 */
float area_piramide(float ab, float al) {
	return ab + al;
}


/**
 * @brief Calcula a area de um cubo
 * @param a Tamanho das arestas do cubo
 * @return Area do cubo
 */
float area_cubo(float a) {
	return 6 * pow(a, 2);
}


/**
 * @brief Calcula a area de um paralelepipedo
 * @param a1 Tamanho de uma aresta do paralelepipedo
 * @param a2 Tamanho de uma aresta do paralelepipedo
 * @param a3 Tamanho de uma aresta do paralelepipedo
 * @return Area do paralelepipedo
 */
float area_paralelepipedo(float a1, float a2, float a3) {
	return (2 * a1 * a2) + (2 * a1 * a3) + (2 * a2 * a3);
}


/**
 * @brief Calcula a area de uma superficie esferica
 * @param r Tamanho do raio da esfera
 * @return Area da superficie esferica
 */
float area_superficie_esferica(float r) {
	return 4 * PI * pow(r, 2);
}
