/**
 * @file	calcarea.cpp
 * @brief	Codigo fonte com a implementacao de funcoes que calculam a area 
 *			de figuras geometricas planas e espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#include "area.h"
#include "calcarea.h"


/**
 * @brief Calculo da area de um triangulo equilatero
 * @param l Tamanho dos lados do triangulo
 */
void calc_area_triangulo(float l) {
	float h = (l / 2) * sqrt(3);		// Calculo da altura
    cout << "Area do triangulo: " << area_triangulo(l, h) << endl;
}


/**
 * @brief Calculo da area de um retangulo
 * @param b Tamanho da base do retangulo
 * @param h Tamanho da altura do retangulo
 */
void calc_area_retangulo(float b, float h){
    cout << "Area do retangulo: " << area_retangulo(b, h) << endl;
}


/**
 * @brief Calculo da area de um quadrado
 * @param l Tamanho dos lados do quadrado
 */
void calc_area_quadrado(float l) {
	cout << "Area do quadrado: " << area_quadrado(l) << endl;
}


/**
 * @brief Calculo da area de um circulo
 * @param r Tamanho do raio do circulo
 */
void calc_area_circulo(float r) {
	cout << "Area do circulo: " << area_circulo(r) << endl;
}


/**
 * @brief Calculo da area de uma piramide de base quadrangular
 * @param ab Tamanho da aresta da base da piramide
 * @param ap Tamanho do apotema das faces laterais da piramide
 */
void calc_area_piramide(float ab, float ap) {
	float area_base = area_quadrado(ab);				// Area da base
	float area_lateral = 4 * area_triangulo(ab, ap);	// Area lateral
	cout << "Area da piramide: " << area_piramide(area_base, area_lateral) << endl;
}


/**
 * @brief Calculo da area de um cubo
 * @param a Tamanho das arestas do cubo
 */
void calc_area_cubo(float a) {
	cout << "Area do cubo: " << area_cubo(a) << endl;
}


/**
 * @brief Calculo da area de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 */
void calc_area_paralelepipedo(float a1, float a2, float a3) {
	cout << "Area do paralelepipedo: " << area_paralelepipedo(a1, a2, a3) << endl;
}


/**
 * @brief Calculo da area de uma superficie esferica
 * @param r Tamanho do raio da superficie esferica
 */
void calc_area_supesferica(float r) {
	cout << "Area da superficie esferica: " << area_superficie_esferica(r) << endl;
}
