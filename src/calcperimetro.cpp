/**
 * @file	calcperimetro.cpp
 * @brief	Codigo fonte com a implementacao de funcoes que realizam o calculo
 *			do perimetro de figuras geometricas planas
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#include "perimetro.h"
#include "calcperimetro.h"


/**
 * @brief Calculo do perimetro de um triangulo equilatero
 * @param l Tamanho dos lados do triangulo
 */
void calc_perimetro_triangulo(float l) {
	cout << "Perimetro do triangulo: " << perimetro_triangulo(l, l, l) << endl;
}


/**
 * @brief Calculo do perimetro de um retangulo
 * @param b Tamanho da base do retangulo
 * @param h Tamanho da altura do retangulo
 */
void calc_perimetro_retangulo(float b, float h) {
	cout << "Perimetro do retangulo: " << perimetro_retangulo(b, h) << endl;
}


/**
 * @brief Calculo do perimetro de um quadrado
 * @param l Tamanho dos lados do quadrado
 */
void calc_perimetro_quadrado(float l) {
	cout << "Perimetro do quadrado: " << perimetro_quadrado(l) << endl;
}


/**
 * @brief Calculo do comprimento da circunferencia de um circulo
 * @param r Tamanho do raio do circulo
 */
void calc_comprimento_circunferencia(float r) {
	cout << "Comprimento da circunferencia do circulo: ";
	cout << comprimento_circunferencia(r) << endl;
}
