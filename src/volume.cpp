/**
 * @file	volume.cpp
 * @brief	Codigo fonte com a implementacao de funcoes que calculam o volume
 *			de figuras geometricas espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#include "volume.h"

/** 
 * @brief Calcula o volume de uma piramide
 * @param ab Area da base da piramide
 * @param h Altura da piramide
 * @return Volume da piramide
 */
float volume_piramide(float ab, float h) {
	return (1.0 / 3.0) * ab * h;
}

/**
 * @brief Calcula o volume de um cubo
 * @param a Tamanho da aresta do cubo
 * @return Volume do cubo
 */
float volume_cubo(float a) {
	return pow(a, 3);
}

/**
 * @brief Calcula o volume de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 * @return Volume do paralelepipedo
 */
float volume_paralelepipedo(float a1, float a2, float a3) {
	return a1 * a2 * a3;
}

/**
 * @brief Calcula o volume de uma esfera
 * @param r Tamanho do raio da esfera
 * @return Volume da esfera
 */
float volume_esfera(float r) {
	return (4.0 / 3.0) * PI * pow(r, 3);
}
