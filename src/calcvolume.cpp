/**
 * @file	calcvolume.cpp
 * @brief	Codigo fonte com a implementacao de funcoes que realizam o calculo
 *			do volume de figuras geometricas espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#include "calcvolume.h"
#include "volume.h"


/**
 * @brief Calculo do volume de uma piramide de base quadrangular
 * @param ab Tamanho da aresta da base da piramide
 * @param h Tamanho da altura da piramide
 */
void calc_volume_piramide(float ab, float h) {
	float area_base = area_quadrado(ab);		// Area da base
	cout << "Volume da piramide: " << volume_piramide(area_base, h) << endl;
}


/**
 * @brief Calculo do volume de um cubo
 * @param a Tamanho das arestas do cubo
 */
void calc_volume_cubo(float a) {
	cout << "Volume do cubo: " << volume_cubo(a) << endl;
}


/**
 * @brief Calculo do volume de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 */
void calc_volume_paralelepipedo(float a1, float a2, float a3) {
	cout << "Volume do paralelepipedo: " << volume_paralelepipedo(a1, a2, a3) << endl;
}


/**
 * @brief Calculo do volume de uma esfera
 * @param r Tamanho do raio da esfera
 */
void calc_volume_esfera(float r) {
	cout << "Volume da esfera: " << volume_esfera(r) << endl;
}
