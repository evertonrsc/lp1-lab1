var searchData=
[
  ['perimetro_5fquadrado',['perimetro_quadrado',['../perimetro_8h.html#a74fe54403fc6f975eef9f5b8e5115023',1,'perimetro_quadrado(float l):&#160;perimetro.cpp'],['../perimetro_8cpp.html#a74fe54403fc6f975eef9f5b8e5115023',1,'perimetro_quadrado(float l):&#160;perimetro.cpp']]],
  ['perimetro_5fretangulo',['perimetro_retangulo',['../perimetro_8h.html#a35e828e64abdbfc1fcd43f83e4173fe3',1,'perimetro_retangulo(float b, float h):&#160;perimetro.cpp'],['../perimetro_8cpp.html#a35e828e64abdbfc1fcd43f83e4173fe3',1,'perimetro_retangulo(float b, float h):&#160;perimetro.cpp']]],
  ['perimetro_5ftriangulo',['perimetro_triangulo',['../perimetro_8h.html#aed42418f0ba0f5c274c6e075f39a8d85',1,'perimetro_triangulo(float l1, float l2, float l3):&#160;perimetro.cpp'],['../perimetro_8cpp.html#aed42418f0ba0f5c274c6e075f39a8d85',1,'perimetro_triangulo(float l1, float l2, float l3):&#160;perimetro.cpp']]]
];
