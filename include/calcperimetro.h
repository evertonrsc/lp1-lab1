/**
 * @file	calcperimetro.h
 * @brief	Definicao dos prototipos de funcoes que realizam o calculo do 
 *		    perimetro de figuras geometricas planas
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */

#ifndef CALC_PERIMETRO_H
#define CALC_PERIMETRO_H

#include <iostream>
using std::cout;
using std::endl;

#include "perimetro.h"


/**
 * @brief Calculo do perimetro de um triangulo equilatero
 * @param l Tamanho dos lados do triangulo
 */
void calc_perimetro_triangulo(float l);

/**
 * @brief Calculo do perimetro de um retangulo
 * @param b Tamanho da base do retangulo
 * @param h Tamanho da altura do retangulo
 */
void calc_perimetro_retangulo(float b, float h);

/**
 * @brief Calculo do perimetro de um quadrado
 * @param l Tamanho dos lados do quadrado
 */
void calc_perimetro_quadrado(float l);

/**
 * @brief Calculo do comprimento da circunferencia de um circulo
 * @param r Tamanho do raio do circulo
 */
void calc_comprimento_circunferencia(float r);

#endif
