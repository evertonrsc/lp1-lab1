/**
 * @file	calcarea.h
 * @brief	Definicao dos prototipos de funcoes que realizam o calculo da area 
 *			de figuras geometricas planas e espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */


#ifndef CALC_AREA_H
#define CALC_AREA_H

#include <iostream>
using std::cout;
using std::endl;

#include "area.h"


/**
 * @brief Calculo da area de um triangulo equilatero
 * @param l Tamanho dos lados do triangulo
 */
void calc_area_triangulo(float l);

/**
 * @brief Calculo da area de um retangulo
 * @param b Tamanho da base do retangulo
 * @param h Tamanho da altura do retangulo
 */
void calc_area_retangulo(float b, float h);

/**
 * @brief Calculo da area de um quadrado
 * @param l Tamanho dos lados do quadrado
 */
void calc_area_quadrado(float l);

/**
 * @brief Calculo da area de um circulo
 * @param r Tamanho do raio do circulo
 */
void calc_area_circulo(float r);

/**
 * @brief Calculo da area de uma piramide de base quadrangular
 * @param ab Tamanho da aresta da base da piramide
 * @param ap Tamanho do apotema das faces laterais da piramide
 */
void calc_area_piramide(float ab, float ap);

/**
 * @brief Calculo da area de um cubo
 * @param a Tamanho das arestas do cubo
 */
void calc_area_cubo(float a);

/**
 * @brief Calculo da area de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 */
void calc_area_paralelepipedo(float a1, float a2, float a3);

/**
 * @brief Calculo da area de uma superficie esferica
 * @param r Tamanho do raio da superficie esferica
 */
void calc_area_supesferica(float r);

#endif
