/**
 * @file	calcvolume.h
 * @brief	Definicao dos prototipos de funcoes que realizam o calculo do 
 *		    volume de figuras geometricas espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */

#ifndef CALC_VOLUME_H
#define CALC_VOLUME_H

#include <iostream>
using std::cout;
using std::endl;

#include "area.h"
#include "volume.h"


/**
 * @brief Calculo do volume de uma piramide de base quadrangular
 * @param ab Tamanho da aresta da base da piramide
 * @param h Tamanho da altura da piramide
 */
void calc_volume_piramide(float ab, float h);

/**
 * @brief Calculo do volume de um cubo
 * @param a Tamanho das arestas do cubo
 */
void calc_volume_cubo(float a);

/**
 * @brief Calculo do volume de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 */
void calc_volume_paralelepipedo(float a1, float a2, float a3);

/**
 * @brief Calculo do volume de uma esfera
 * @param r Tamanho do raio da esfera
 */
void calc_volume_esfera(float r);

#endif
