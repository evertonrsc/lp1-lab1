/**
 * @file	perimetro.h
 * @brief	Declaracao dos prototipos de funcoes que calculam o perimetro de
 *		    figuras geometricas planas
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */

#ifndef PERIMETRO_H
#define PERIMETRO_H

/** @brief Valor para a constante &pi; */
#define PI 3.1415


/** 
 * @brief Calcula o perimetro de um triangulo 
 * @param l1 Tamanho do lado do triangulo
 * @param l2 Tamanho do lado do triangulo
 * @param l3 Tamanho do lado do triangulo
 * @return Perimetro do triangulo
 */
float perimetro_triangulo(float l1, float l2, float l3);


/** 
 * @brief Calcula o perimetro de um retangulo
 * @param b Tamanho da base do retangulo
 * @param h Tamanho da altura do retangulo
 * @return Perimetro do retangulo
 */
float perimetro_retangulo(float b, float h);


/** 
 * @brief Calcula o perimetro de um quadrado
 * @param l Tamanho do lado do quadrado
 * @return Perimetro do quadrado
 */
float perimetro_quadrado(float l);


/** 
 * @brief Calcula o comprimento da circunferencia de um circulo
 * @param r Tamanho do raio do circulo
 * @return Comprimento da circunferencia do circulo
 */
float comprimento_circunferencia(float r);

#endif
