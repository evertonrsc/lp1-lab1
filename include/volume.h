/**
 * @file	volume.h
 * @brief	Declaracao dos prototipos de funcoes que calculam o volume de 
 *		    figuras geometricas espaciais
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	15/03/2017
 * @date	15/03/2017
 */

#ifndef VOLUME_H
#define VOLUME_H

#include <cmath>
using std::pow;

/** @brief Valor para a constante &pi; */
#define PI 3.1415


/** 
 * @brief Calcula o volume de uma piramide
 * @param ab Medida da area da base da piramide
 * @param h Tamanho da altura da piramide
 * @return Volume da piramide
 */
float volume_piramide(float ab, float h);

/**
 * @brief Calcula o volume de um cubo
 * @param a Tamanho da aresta do cubo
 * @return Volume do cubo
 */
float volume_cubo(float a);

/**
 * @brief Calcula o volume de um paralelepipedo
 * @param a1 Tamanho de aresta do paralelepipedo
 * @param a2 Tamanho de aresta do paralelepipedo
 * @param a3 Tamanho de aresta do paralelepipedo
 * @return Volume do paralelepipedo
 */
float volume_paralelepipedo(float a1, float a2, float a3);

/**
 * @brief Calcula o volume de uma esfera
 * @param r Tamanho do raio da esfera
 * @return Volume da esfera
 */
float volume_esfera(float r);

#endif
